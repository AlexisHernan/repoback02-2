package com.alexis.servicios;

import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;
import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")  //Atención

public class ServiciosController {
    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServicioService.getFiltrados(filtro);
    }

    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio) {
        try {
            ServicioService.insert(newServicio);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ServicioService.update(filtro,updates);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}





